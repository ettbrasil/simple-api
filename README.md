**ETTBrasil SimpleAPI makes it easy to send HTTP requests and to integrate with Zend Server API.**

Simple interface for building requests and getting responses in XML & JSON

[Repository](https://bitbucket.org/ettbrasil/simpleapi)

Installing SimpleAPI

The recommended way to install ETTBrasil SimpleAPI is through [Composer](http://getcomposer.org).
```bash
## Install Composer
curl -sS https://getcomposer.org/installer | php
```

Next, run the Composer command to install the latest stable version of ETTBrasil SimpleAPI:
```bash
php composer.phar require ettbrasil/zend-simple-api
```

After installing, you need to require Composer's autoloader:
```bash
require 'vendor/autoload.php';
```

You can then later update SimpleAPI using composer:
```bash
composer.phar update
```
